import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class ServService {
  constructor(private http: Http) {}

  getUsersObservable() {
    return this.http.get("http://localhost:3000/contacts").map(odgovor => {
      return odgovor.json();
    });
  }

  getSingleUser(id) {
    return this.http
      .get("http://localhost:3000/contacts/" + id)
      .map(odgovor => {
        return odgovor.json();
      });
  }

  addUser(user) {
    return this.http
      .post("http://localhost:3000/contacts", user)
      .map(odgovor => {
        return odgovor.json();
      });
  }

  updateUser(user) {
    return this.http
      .patch("http://localhost:3000/contacts/" + user.id, user)
      .map(odgovor => {
        return odgovor.json();
      });
  }

  deleteUser(id) {
    return this.http
      .delete("http://localhost:3000/contacts/" + id)
      .map(odgovor => {
        return odgovor.json();
      });
  }
}
