import { Injectable } from "@angular/core";
import { ServService } from "./serv.service";
import { MatTableDataSource } from "@angular/material";

@Injectable()
export class ModelService {
  getUserObservable: any;
  users = [];
  usersDataSource = new MatTableDataSource();

  constructor(private service: ServService) {
    this.refreshUsers();
  }

  refreshUsers() {
    this.service.getUsersObservable().subscribe(users => {
      this.users = users
      this.usersDataSource.data = this.users;
    });
  }

  refreshUsersWithClbk(clbk) {
    this.service.getUsersObservable().subscribe(users => {
      this.users = users;
      this.usersDataSource.data = this.users;
      clbk();
    });
  }

  addNewUser(user, clbk) {
    this.service.addUser(user).subscribe(response => {
      this.users.push(response);
      this.usersDataSource.data = this.users;
      clbk();
    });
  }

  getSingleUser(id, clbk) {
    if (this.users && this.users.length > 0) {
      for (var i = 0; i < this.users.length; i++) {
        if (parseInt(this.users[i].id) === parseInt(id)) {
          clbk(this.users[i]);
        }
      }
    } else {
      this.refreshUsersWithClbk(() => {
        for (var i = 0; i < this.users.length; i++) {
          if (parseInt(this.users[i].id) === parseInt(id)) {
            clbk(this.users[i]);
          }
        }
      });
    }
  }

  updateUser(user, clbk) {
    this.service.updateUser(user).subscribe(user => {
      this.refreshUsers();
      clbk();
    });
  }

  deleteUser(id, clbk) {
    this.service.deleteUser(id).subscribe(() => {
      this.refreshUsers();
      clbk();
    });
  }
}
