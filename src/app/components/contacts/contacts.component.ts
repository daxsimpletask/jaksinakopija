import { Component, OnInit, ViewChild } from "@angular/core";
import { ModelService } from "../../service/model.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html"
})
export class ContactsComponent implements OnInit {

  displayedColumns = [
    'openButton',
    'id',
    'firstName',
    'lastName',
    'img',
    'age',
    'birthDate',
    'editButton',
    'removeButton'
  ];

  constructor(
    public model: ModelService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngAfterViewInit() {
    this.model.usersDataSource.paginator = this.paginator;
    this.model.usersDataSource.sort = this.sort;
  }

  applyFilter(filterValue) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.model.usersDataSource.filter = filterValue;
  }

  showSingleUser(id) {
    this.router.navigate(["/contact", id]);
  }

  editUser(id) {
    this.router.navigate(["/contact", id, "edit"]);
  }

  deleteUser(id) {
    this.model.deleteUser(id, () => {
      this.router.navigate(["/contacts"]);
    });
  }

  ngOnInit() { }
}
