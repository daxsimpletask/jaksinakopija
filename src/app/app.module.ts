import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MyDatePickerModule } from "../../node_modules/angular4-datepicker/src/my-date-picker/my-date-picker.module";

import { AppComponent } from "./app.component";
import { ContactsComponent } from "./components/contacts/contacts.component";
import { NewComponent } from "./components/new/new.component";
import { SingleContactComponent } from "./components/single-contact/single-contact.component";
import { EditComponent } from "./components/edit/edit.component";
import { InfoComponent } from "./components/info/info.component";
import { HomeComponent } from "./components/home/home.component";

import { FormsModule } from "@angular/forms";

import { ModelService } from "./service/model.service";
import { ServService } from "./service/serv.service";
import { SearchNamePipePipe } from "./pipes/search-name-pipe.pipe";

const rute: Routes = [
  { path: "", component: HomeComponent },
  { path: "contacts", component: ContactsComponent },
  { path: "contacts/new", component: NewComponent },
  {
    path: "contact/:id",
    component: SingleContactComponent,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: EditComponent },
      { path: "info", component: InfoComponent }
    ]
  },
  { path: "**", redirectTo: "/" }
];

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    NewComponent,
    SingleContactComponent,
    EditComponent,
    InfoComponent,
    HomeComponent,
    SearchNamePipePipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rute),
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    MyDatePickerModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule
  ],
  providers: [ServService, ModelService],
  bootstrap: [AppComponent]
})
export class AppModule {}
